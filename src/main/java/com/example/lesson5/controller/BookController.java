package com.example.lesson5.controller;

import com.example.lesson5.dto.BookRequestDto;
import com.example.lesson5.dto.BookResponseDto;
import com.example.lesson5.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;
    @GetMapping
    public ResponseEntity<BookResponseDto> get(@PathVariable Integer id){
      BookResponseDto responseDto=  bookService.getId(id);
        return ResponseEntity.ok(responseDto);
    }
    @PostMapping()
    public ResponseEntity<Void> create(BookRequestDto bookRequestDto){
      int id=  bookService.create(bookRequestDto);
        return ResponseEntity.created(URI.create("book/"+id)).build();

    }
    @PutMapping("{id}")
    public ResponseEntity<BookResponseDto>update(@PathVariable Integer id
    , @RequestBody BookRequestDto bookRequestDto){
       BookResponseDto response= bookService.update(id,bookRequestDto);
        return ResponseEntity.ok(response);
    }
    @DeleteMapping
    public ResponseEntity<Void> delete(@PathVariable Integer id){
        bookService.delete(id);
        return ResponseEntity.ok().build();
    }
}
