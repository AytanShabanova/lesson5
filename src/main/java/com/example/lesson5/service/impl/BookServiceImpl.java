package com.example.lesson5.service.impl;

import com.example.lesson5.dto.BookRequestDto;
import com.example.lesson5.dto.BookResponseDto;
import com.example.lesson5.model.Book;
import com.example.lesson5.repo.BookRepository;
import com.example.lesson5.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    @Override
    public int create(BookRequestDto bookRequestDto) {
       Book book=Book.builder().
               name(bookRequestDto.getName()).
               author(bookRequestDto.getAuthor()).
               pageCount(bookRequestDto.getPageCount()).
               build();
       bookRepository.save(book);
       return book.getId();
    }

    @Override
    public BookResponseDto update(Integer id, BookRequestDto bookRequestDto) {
        Book book1=bookRepository.findById(id).orElseThrow(() -> new RuntimeException("BOOK NOT FOUND"));
        book1.setName(bookRequestDto.getName());
        book1.setAuthor(bookRequestDto.getAuthor());
        book1.setPageCount(bookRequestDto.getPageCount());
        Book book2=bookRepository.save(book1);
        return BookResponseDto.builder().name(book2.getName()).author(book2.getAuthor()).pageCount(book2.getPageCount()).build();


    }

    @Override
    public void delete(Integer id) {
 bookRepository.deleteById(id);
    }

    @Override
    public BookResponseDto getId(Integer id) {
       Book book= bookRepository.findById(id).orElseThrow(() -> new RuntimeException("BOOK NOT FOUND"));
      return BookResponseDto.builder().name(book.getName()).author(book.getAuthor()).pageCount(book.getPageCount()).build();

    }
}
