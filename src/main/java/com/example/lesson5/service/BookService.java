package com.example.lesson5.service;

import com.example.lesson5.dto.BookRequestDto;
import com.example.lesson5.dto.BookResponseDto;

public interface BookService {
    int create(BookRequestDto bookRequestDto);

    BookResponseDto update(Integer id, BookRequestDto bookRequestDto);

    void delete(Integer id);

    BookResponseDto getId(Integer id);
}
